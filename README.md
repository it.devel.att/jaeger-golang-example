#### Jaeger tracing example

Article's with help:
* https://eax.me/golang-jaeger/
* https://opentracing.io/guides/golang/quick-start/
* https://opencensus.io/exporters/supported-exporters/go/jaeger/

Start jaeger all in one
```shell script
docker-compose up
```

In few terminals launch servers

```shell script
go run cmd/server1/main.go
```
```shell script
go run cmd/server2/main.go
```
```shell script
go run cmd/server3/main.go
```
```shell script
go run cmd/server4/main.go
```

Make request to first server with curl
```shell script
curl localhost:8000/api/v1/dostuff -v
```

Open [Jaeger UI](http://localhost:16686/) and watch traces

![jaeger-example-ui](/docs/img/jaeger-ui.png)
![jaeger-example-ui](/docs/img/jaeger-arch-example.png)
