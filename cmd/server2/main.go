package main

import (
	"context"
	"net/http"
	"time"

	"go.opencensus.io/trace"

	"jaeger-example/pkg/middleware"
	"jaeger-example/pkg/utils"
)

func main() {
	serviceName := "server-2"
	utils.InitJaeger(serviceName, "localhost:6831")
	traceMiddleware := middleware.NewTraceMiddleware(serviceName)

	mux := http.NewServeMux()
	mux.HandleFunc("/api/v1/posts", Posts)

	go http.ListenAndServe(":8001", traceMiddleware.WrapHandler(mux))
	utils.WaitForStop()
}

func Posts(writer http.ResponseWriter, request *http.Request) {
	ctx, span := trace.StartSpan(request.Context(), "posts")
	defer span.End()
	sendRequestToCommentsService(ctx)
	sendRequestToRatesService(ctx)

	time.Sleep(time.Second)
	writer.WriteHeader(http.StatusOK)
}

func sendRequestToCommentsService(parentCtx context.Context) error {
	ctx, span := trace.StartSpan(parentCtx, "sendRequestToCommentsService")
	defer span.End()

	_, err := utils.SendTraceRequest(ctx, "GET", "http://localhost:8002/api/v1/comments", nil)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return err
	}
	return nil
}

func sendRequestToRatesService(parentCtx context.Context) error {
	ctx, span := trace.StartSpan(parentCtx, "sendRequestToRatesService")
	defer span.End()

	_, err := utils.SendTraceRequest(ctx, "GET", "http://localhost:8003/api/v1/rates", nil)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return err
	}
	return nil
}
