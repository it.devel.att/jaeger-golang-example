package main

import (
	"net/http"
	"time"

	"go.opencensus.io/trace"

	"jaeger-example/pkg/middleware"
	"jaeger-example/pkg/utils"
)

func main() {
	serviceName := "server-4"
	utils.InitJaeger(serviceName, "localhost:6831")
	traceMiddleware := middleware.NewTraceMiddleware(serviceName)

	mux := http.NewServeMux()
	mux.HandleFunc("/api/v1/rates", Rates)

	go http.ListenAndServe(":8003", traceMiddleware.WrapHandler(mux))
	utils.WaitForStop()
}

func Rates(writer http.ResponseWriter, request *http.Request) {
	_, span := trace.StartSpan(request.Context(), "rates")
	defer span.End()

	time.Sleep(time.Millisecond * 20)
	writer.WriteHeader(http.StatusOK)
}
