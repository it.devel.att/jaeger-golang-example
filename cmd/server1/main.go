package main

import (
	"context"
	"net/http"
	"time"

	"go.opencensus.io/trace"

	"jaeger-example/pkg/middleware"
	"jaeger-example/pkg/utils"
)

func main() {
	serviceName := "server-1"
	utils.InitJaeger(serviceName, "localhost:6831")
	traceMiddleware := middleware.NewTraceMiddleware(serviceName)

	mux := http.NewServeMux()
	mux.HandleFunc("/api/v1/dostuff", DoStuff)

	go http.ListenAndServe(":8000", traceMiddleware.WrapHandler(mux))
	utils.WaitForStop()
}

func DoStuff(writer http.ResponseWriter, request *http.Request) {
	ctx, span := trace.StartSpan(request.Context(), "doStuff")
	defer span.End()
	time.Sleep(time.Millisecond * 20)

	err := sendRequestToPostsService(ctx)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte(err.Error()))
		return
	}
	writer.WriteHeader(http.StatusOK)
}

func sendRequestToPostsService(parentCtx context.Context) error {
	ctx, span := trace.StartSpan(parentCtx, "sendRequestToPostsService")
	defer span.End()

	_, err := utils.SendTraceRequest(ctx, "GET", "http://localhost:8001/api/v1/posts", nil)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return err
	}
	return nil
}
