package main

import (
	"net/http"
	"time"

	"go.opencensus.io/trace"

	"jaeger-example/pkg/middleware"
	"jaeger-example/pkg/utils"
)

func main() {
	serviceName := "server-3"
	utils.InitJaeger(serviceName, "localhost:6831")
	traceMiddleware := middleware.NewTraceMiddleware(serviceName)

	mux := http.NewServeMux()
	mux.HandleFunc("/api/v1/comments", Comments)

	go http.ListenAndServe(":8002", traceMiddleware.WrapHandler(mux))
	utils.WaitForStop()
}

func Comments(writer http.ResponseWriter, request *http.Request) {
	_, span := trace.StartSpan(request.Context(), "comments")
	defer span.End()

	time.Sleep(time.Millisecond * 30)
	writer.WriteHeader(http.StatusOK)
}
