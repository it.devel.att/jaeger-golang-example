package utils

import (
	"log"

	"contrib.go.opencensus.io/exporter/jaeger"
	"go.opencensus.io/trace"
)

func InitJaeger(serviceName string, agentEndpoint string) {
	exporter, err := jaeger.NewExporter(jaeger.Options{
		AgentEndpoint: agentEndpoint,
		Process: jaeger.Process{
			ServiceName: serviceName,
			Tags: []jaeger.Tag{
				jaeger.StringTag("environment", "production"),
			},
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{
		// TODO Just for example trace all!
		DefaultSampler: trace.AlwaysSample(),
	})
}
