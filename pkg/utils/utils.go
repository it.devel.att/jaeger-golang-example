package utils

import (
	"os"
	"os/signal"
	"syscall"
)

func WaitForStop() {
	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)
	<-sigCh
}
