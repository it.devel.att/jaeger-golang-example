package utils

import (
	"context"
	"io"

	"go.opencensus.io/trace"
	"net/http"
)

func SendTraceRequest(parentCtx context.Context, method string, url string, body io.Reader) (*http.Response, error) {
	ctx, span := trace.StartSpan(parentCtx, "SendTraceRequest")
	defer span.End()

	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return nil, err
	}
	SetTraceHeader(req, span)

	span.AddAttributes(
		trace.StringAttribute("method", req.Method),
		trace.StringAttribute("url", req.URL.String()),
	)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		span.AddAttributes(trace.StringAttribute("error", err.Error()))
		return nil, err
	}
	return res, nil
}
