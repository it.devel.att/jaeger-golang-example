package utils

import (
	"encoding/json"
	"net/http"

	"go.opencensus.io/trace"
)

const TraceHeader = "X-Trace-Span-Context"

func SetTraceHeader(req *http.Request, span *trace.Span) {
	spanContextJson, _ := json.Marshal(span.SpanContext())
	req.Header.Set(TraceHeader, string(spanContextJson))
}

func FetchTraceSpanContext(req *http.Request) trace.SpanContext {
	spanContextJson := req.Header.Get(TraceHeader)
	var spanContext trace.SpanContext
	_ = json.Unmarshal([]byte(spanContextJson), &spanContext)
	return spanContext
}
