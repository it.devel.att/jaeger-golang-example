package middleware

import (
	"net/http"

	"go.opencensus.io/trace"
	"jaeger-example/pkg/utils"
)

type (
	Middleware interface {
		WrapHandler(next http.Handler) http.Handler
	}
	traceMiddleware struct {
		serviceName string
	}
)

func NewTraceMiddleware(serviceName string) Middleware {
	return newTraceMiddleware(serviceName)
}

func newTraceMiddleware(serviceName string) *traceMiddleware {
	return &traceMiddleware{serviceName: serviceName}
}

func (m *traceMiddleware) WrapHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		spanAttributes := []trace.Attribute{
			trace.StringAttribute("method", r.Method),
			trace.StringAttribute("path", r.URL.RequestURI()),
			trace.StringAttribute("remote_addr", r.RemoteAddr),
			trace.StringAttribute("user_agent", r.UserAgent()),
		}
		spanContext := utils.FetchTraceSpanContext(r)

		ctx, span := trace.StartSpanWithRemoteParent(r.Context(), m.serviceName, spanContext)
		span.AddAttributes(spanAttributes...)
		req := r.WithContext(ctx)
		defer span.End()

		next.ServeHTTP(w, req)
	})
}
